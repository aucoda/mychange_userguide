//  FaqTopic definition
    "use strict";
    const au_node = require('../lib/au_node');
const path = require('path');
    let globals = null;
    class FaqTopic  {
        static setGlobals(_globals) {
            globals = _globals;
        }       
        constructor() {
            this.topic = "";
this.active = false;
        }
        mapping (mapFaqTopic) {if(mapFaqTopic) {this.topic = mapFaqTopic.___data__ ? mapFaqTopic.___data__.topic : mapFaqTopic.topic;
this.active = mapFaqTopic.___data__ ? mapFaqTopic.___data__.active : mapFaqTopic.active;}}
    }
    module.exports = FaqTopic;