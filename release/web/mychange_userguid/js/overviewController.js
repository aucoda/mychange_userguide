        //  _overviewController definition",
        class _overviewController  {
        constructor () {
            this.seeMoreOnlineFormRFC = false;this.seeMoreOnlineFormIA = false;this.RFCSection_visibile = false;this.RFCDetailsIAS_visibile = false;this.RFCDetailsDS_visibile = false;this.RFCDetailsFS_visibile = false;this.RFCManagementScreensTable_visibile = false;this.onlineFormsRFC_visible = false;this.onlineFormsIA_visible = false;this.onlineFormsCAN_visible = false;this.minorChangeIAForm_visible = false;this.otherSupportingDocuments_visible = false;this.documentManagementRFC_visible = false;this.documentManagementIA_visible = false;this.documentManagementCAN_visible = false;this.referenceDocuments_visible = false;this.attachingDocument_visible = false;this.privacyByDesign_visible = false;this.pricingTables_visible = false;        }
          load () {loadView("mychange_userguidUI", "overview");
this.addHeaderWidget();
this.addUserGuideMenu();
document.getElementById("lbl_onlineFormsRFC_Inner2").style.display = 'none';
document.getElementById("lbl_onlineFormsIA_Inner2").style.display = 'none';}  addHeaderWidget () {headerWidget_remove();
small_headerWidget_remove();
let headerW = headerWidget_create("Change Guidance", "overviewHeaderPlaceholder");
let small_headerW = small_headerWidget_create("Change Guidance", "small_overviewHeaderPlaceholder");}  addUserGuideMenu () {userGuideMenuWidget_remove();
let userGuideMenuW = userGuideMenuWidget_create(true, "overviewUserGuideMenuPlaceholder");
document.getElementById(userGuideMenuW.controlID("guidanceOptions_UGMW")).style.display = 'none';
document.getElementById(userGuideMenuW.controlID("myChangeOverview_UGMW")).style.backgroundColor = "#E6E6E6";}  showOverlay (overviewID) {overlay("mychange_userguidUI", "overviewOverlay");
this.hideOverviewSections();
if ( overviewID === 1 ) {document.getElementById("RFCListsContainer").style.display = 'flex';
document.getElementById("lbl_overviewOverlay").innerHTML = "RFC Lists";}else if ( overviewID === 2 ) {document.getElementById("RFCManagementContainer").style.display = 'flex';
document.getElementById("lbl_overviewOverlay").innerHTML = "RFC Management";}else if ( overviewID === 3 ) {document.getElementById("onlineFormsContainer").style.display = 'flex';
document.getElementById("lbl_overviewOverlay").innerHTML = "Online Forms";}else if ( overviewID === 4 ) {document.getElementById("documentManagementContainer").style.display = 'flex';
document.getElementById("lbl_overviewOverlay").innerHTML = "Document Management";}else if ( overviewID === 5 ) {document.getElementById("notificationsContainer").style.display = 'flex';
document.getElementById("lbl_overviewOverlay").innerHTML = "Notifications";}else if ( overviewID === 6 ) {document.getElementById("DashboardsContainer").style.display = 'flex';
document.getElementById("lbl_overviewOverlay").innerHTML = "Dashboards";}}  hideOverlay () {remove("mychange_userguidUI", "overviewOverlay");
this.hideInnerCollapseSections_RFCManagement();
this.hideInnerCollapseSections_onlineForms();
this.hideInnerCollapseSections_documentManagement();
document.getElementById("lbl_onlineFormsRFC_SeeMore").innerHTML = "See more";this.seeMoreOnlineFormRFC = false;document.getElementById("lbl_onlineFormsRFC_Inner2").style.display = 'none';
document.getElementById("lbl_onlineFormsIA_SeeMore").innerHTML = "See more";this.seeMoreOnlineFormIA = false;document.getElementById("lbl_onlineFormsIA_Inner2").style.display = 'none';}  hideOverviewSections () {document.getElementById("RFCListsContainer").style.display = 'none';
document.getElementById("RFCManagementContainer").style.display = 'none';
document.getElementById("onlineFormsContainer").style.display = 'none';
document.getElementById("documentManagementContainer").style.display = 'none';
document.getElementById("notificationsContainer").style.display = 'none';
document.getElementById("DashboardsContainer").style.display = 'none';}  showInnerCollapseSection_RFCManagement (sectionID) {if ( sectionID === 1 ) {if ( this.RFCSection_visibile === false ) {document.getElementById("RFCSection_Inner").style.display = 'block';
document.getElementById("img_RFCSection").src = "./Images/remove_circle_outline_black.svg";this.RFCSection_visibile = true;}else{document.getElementById("RFCSection_Inner").style.display = 'none';
document.getElementById("img_RFCSection").src = "./Images/add_circle_outline_black.svg";this.RFCSection_visibile = false;}}else if ( sectionID === 2 ) {if ( this.RFCDetailsIAS_visibile === false ) {document.getElementById("RFCDetailsIAS_Inner").style.display = 'block';
document.getElementById("img_RFCDetailsIAS").src = "./Images/remove_circle_outline_black.svg";this.RFCDetailsIAS_visibile = true;}else{document.getElementById("RFCDetailsIAS_Inner").style.display = 'none';
document.getElementById("img_RFCDetailsIAS").src = "./Images/add_circle_outline_black.svg";this.RFCDetailsIAS_visibile = false;}}else if ( sectionID === 3 ) {if ( this.RFCDetailsDS_visibile === false ) {document.getElementById("RFCDetailsDS_Inner").style.display = 'block';
document.getElementById("img_RFCDetailsDS").src = "./Images/remove_circle_outline_black.svg";this.RFCDetailsDS_visibile = true;}else{document.getElementById("RFCDetailsDS_Inner").style.display = 'none';
document.getElementById("img_RFCDetailsDS").src = "./Images/add_circle_outline_black.svg";this.RFCDetailsDS_visibile = false;}}else if ( sectionID === 4 ) {if ( this.RFCDetailsFS_visibile === false ) {document.getElementById("RFCDetailsFS_Inner").style.display = 'block';
document.getElementById("img_RFCDetailsFS").src = "./Images/remove_circle_outline_black.svg";this.RFCDetailsFS_visibile = true;}else{document.getElementById("RFCDetailsFS_Inner").style.display = 'none';
document.getElementById("img_RFCDetailsFS").src = "./Images/add_circle_outline_black.svg";this.RFCDetailsFS_visibile = false;}}else if ( sectionID === 5 ) {if ( this.RFCManagementScreensTable_visibile === false ) {document.getElementById("RFCManagementScreensTable_Inner").style.display = 'block';
document.getElementById("img_RFCManagementScreensTable").src = "./Images/remove_circle_outline_black.svg";}else{document.getElementById("RFCManagementScreensTable_Inner").style.display = 'none';
document.getElementById("img_RFCManagementScreensTable").src = "./Images/add_circle_outline_black.svg";this.RFCManagementScreensTable_visibile = false;}}}  hideInnerCollapseSections_RFCManagement () {document.getElementById("RFCSection_Inner").style.display = 'none';
document.getElementById("RFCDetailsIAS_Inner").style.display = 'none';
document.getElementById("RFCDetailsDS_Inner").style.display = 'none';
document.getElementById("RFCDetailsFS_Inner").style.display = 'none';
document.getElementById("RFCManagementScreensTable_Inner").style.display = 'none';
document.getElementById("img_RFCSection").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_RFCDetailsIAS").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_RFCDetailsDS").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_RFCDetailsFS").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_RFCManagementScreensTable").src = "./Images/add_circle_outline_black.svg";}  showInnerCollapseSection_onlineForms (sectionID) {if ( sectionID === 1 ) {if ( this.onlineFormsRFC_visible === false ) {document.getElementById("onlineFormsRFC_Inner").style.display = 'block';
document.getElementById("img_onlineFormsRFC").src = "./Images/remove_circle_outline_black.svg";this.onlineFormsRFC_visible = true;}else{document.getElementById("onlineFormsRFC_Inner").style.display = 'none';
document.getElementById("img_onlineFormsRFC").src = "./Images/add_circle_outline_black.svg";this.onlineFormsRFC_visible = false;}}else if ( sectionID === 2 ) {if ( this.onlineFormsIA_visible === false ) {document.getElementById("onlineFormsIA_Inner").style.display = 'block';
document.getElementById("img_onlineFormsIA").src = "./Images/remove_circle_outline_black.svg";this.onlineFormsIA_visible = true;}else{document.getElementById("onlineFormsIA_Inner").style.display = 'none';
document.getElementById("img_onlineFormsIA").src = "./Images/add_circle_outline_black.svg";this.onlineFormsIA_visible = false;}}else if ( sectionID === 3 ) {if ( this.onlineFormsCAN_visible === false ) {document.getElementById("onlineFormsCAN_Inner").style.display = 'block';
document.getElementById("img_onlineFormsCAN").src = "./Images/remove_circle_outline_black.svg";this.onlineFormsCAN_visible = true;}else{document.getElementById("onlineFormsCAN_Inner").style.display = 'none';
document.getElementById("img_onlineFormsCAN").src = "./Images/add_circle_outline_black.svg";this.onlineFormsCAN_visible = false;}}else if ( sectionID === 4 ) {if ( this.minorChangeIAForm_visible === false ) {document.getElementById("minorChangeIAForm_Inner").style.display = 'block';
document.getElementById("img_minorChangeIAForm").src = "./Images/remove_circle_outline_black.svg";this.minorChangeIAForm_visible = true;}else{document.getElementById("minorChangeIAForm_Inner").style.display = 'none';
document.getElementById("img_minorChangeIAForm").src = "./Images/add_circle_outline_black.svg";this.minorChangeIAForm_visible = false;}}else if ( sectionID === 5 ) {if ( this.otherSupportingDocuments_visible === false ) {document.getElementById("otherSupportingDocuments_Inner").style.display = 'block';
document.getElementById("img_otherSupportingDocuments").src = "./Images/remove_circle_outline_black.svg";this.otherSupportingDocuments_visible = true;}else{document.getElementById("otherSupportingDocuments_Inner").style.display = 'none';
document.getElementById("img_otherSupportingDocuments").src = "./Images/add_circle_outline_black.svg";this.otherSupportingDocuments_visible = false;}}}  hideInnerCollapseSections_onlineForms () {document.getElementById("onlineFormsRFC_Inner").style.display = 'none';
document.getElementById("onlineFormsIA_Inner").style.display = 'none';
document.getElementById("onlineFormsCAN_Inner").style.display = 'none';
document.getElementById("minorChangeIAForm_Inner").style.display = 'none';
document.getElementById("otherSupportingDocuments_Inner").style.display = 'none';
document.getElementById("img_onlineFormsRFC").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_onlineFormsIA").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_onlineFormsCAN").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_minorChangeIAForm").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_otherSupportingDocuments").src = "./Images/add_circle_outline_black.svg";}  showInnerCollapseSection_documentManagement (sectionID) {if ( sectionID === 1 ) {if ( this.documentManagementRFC_visible === false ) {document.getElementById("documentManagementRFC_Inner").style.display = 'block';
document.getElementById("img_documentManagementRFC").src = "./Images/remove_circle_outline_black.svg";this.documentManagementRFC_visible = true;}else{document.getElementById("documentManagementRFC_Inner").style.display = 'none';
document.getElementById("img_documentManagementRFC").src = "./Images/add_circle_outline_black.svg";this.documentManagementRFC_visible = false;}}else if ( sectionID === 2 ) {if ( this.documentManagementIA_visible === false ) {document.getElementById("documentManagementIA_Inner").style.display = 'block';
document.getElementById("img_documentManagementIA").src = "./Images/remove_circle_outline_black.svg";this.documentManagementIA_visible = true;}else{document.getElementById("documentManagementIA_Inner").style.display = 'none';
document.getElementById("img_documentManagementIA").src = "./Images/add_circle_outline_black.svg";this.documentManagementIA_visible = false;}}else if ( sectionID === 3 ) {if ( this.documentManagementCAN_visible === false ) {document.getElementById("documentManagementCAN_Inner").style.display = 'block';
document.getElementById("img_documentManagementCAN").src = "./Images/remove_circle_outline_black.svg";this.documentManagementCAN_visible = true;}else{document.getElementById("documentManagementCAN_Inner").style.display = 'none';
document.getElementById("img_documentManagementCAN").src = "./Images/add_circle_outline_black.svg";this.documentManagementCAN_visible = false;}}else if ( sectionID === 4 ) {if ( this.referenceDocuments_visible === false ) {document.getElementById("referenceDocuments_Inner").style.display = 'block';
document.getElementById("img_referenceDocuments").src = "./Images/remove_circle_outline_black.svg";this.referenceDocuments_visible = true;}else{document.getElementById("referenceDocuments_Inner").style.display = 'none';
document.getElementById("img_referenceDocuments").src = "./Images/add_circle_outline_black.svg";this.referenceDocuments_visible = false;}}else if ( sectionID === 5 ) {if ( this.attachingDocument_visible === false ) {document.getElementById("attachingDocument_Inner").style.display = 'block';
document.getElementById("img_attachingDocument").src = "./Images/remove_circle_outline_black.svg";this.attachingDocument_visible = true;}else{document.getElementById("attachingDocument_Inner").style.display = 'none';
document.getElementById("img_attachingDocument").src = "./Images/add_circle_outline_black.svg";this.attachingDocument_visible = false;}}else if ( sectionID === 6 ) {if ( this.privacyByDesign_visible === false ) {document.getElementById("privacyByDesign_Inner").style.display = 'block';
document.getElementById("img_privacyByDesign").src = "./Images/remove_circle_outline_black.svg";this.privacyByDesign_visible = true;}else{document.getElementById("privacyByDesign_Inner").style.display = 'none';
document.getElementById("img_privacyByDesign").src = "./Images/add_circle_outline_black.svg";this.privacyByDesign_visible = false;}}else if ( sectionID === 7 ) {if ( this.pricingTables_visible === false ) {document.getElementById("pricingTables_Inner").style.display = 'block';
document.getElementById("img_pricingTables").src = "./Images/remove_circle_outline_black.svg";this.pricingTables_visible = true;}else{document.getElementById("pricingTables_Inner").style.display = 'none';
document.getElementById("img_pricingTables").src = "./Images/add_circle_outline_black.svg";this.pricingTables_visible = false;}}}  hideInnerCollapseSections_documentManagement () {document.getElementById("documentManagementRFC_Inner").style.display = 'none';
document.getElementById("documentManagementIA_Inner").style.display = 'none';
document.getElementById("documentManagementCAN_Inner").style.display = 'none';
document.getElementById("referenceDocuments_Inner").style.display = 'none';
document.getElementById("attachingDocument_Inner").style.display = 'none';
document.getElementById("privacyByDesign_Inner").style.display = 'none';
document.getElementById("pricingTables_Inner").style.display = 'none';
document.getElementById("img_documentManagementRFC").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_documentManagementIA").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_documentManagementCAN").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_referenceDocuments").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_attachingDocument").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_privacyByDesign").src = "./Images/add_circle_outline_black.svg";document.getElementById("img_pricingTables").src = "./Images/add_circle_outline_black.svg";} async downloadFile (type) {let f = new Blob([""]);
if ( type === 1 ) {let  responseText_xCZhKHdm = await MyChangePortalWs.downloadFileUserGuideWf ("RFC Raised Detailed Guidance.docx");f = JSON.parse(responseText_xCZhKHdm);}else if ( type === 2 ) {let  responseText_jJvbnrSg = await MyChangePortalWs.downloadFileUserGuideWf ("3RD PARTY IA TEMPLATE.docx");f = JSON.parse(responseText_jJvbnrSg);}} async downloadBlankRFC () {let f = new Blob([""]);
let name = "";
if ( UserRole.indexOf("SSCL") !== -1 ) {name = "BLANK RFC SSCL.docx";}else if ( UserRole.indexOf("MPS") !== -1 ) {name = "BLANK RFC MPS.docx";}let  responseText_em6D0lSY = await MyChangePortalWs.downloadFileUserGuideWf (name);f = JSON.parse(responseText_em6D0lSY);}  seeMoreOnlineFormsRFC () {if ( this.seeMoreOnlineFormRFC ) {document.getElementById("lbl_onlineFormsRFC_Inner2").style.display = 'none';
document.getElementById("lbl_onlineFormsRFC_SeeMore").innerHTML = "See more";this.seeMoreOnlineFormRFC = false;}else{document.getElementById("lbl_onlineFormsRFC_Inner2").style.display = '';
document.getElementById("lbl_onlineFormsRFC_SeeMore").innerHTML = "See less";this.seeMoreOnlineFormRFC = true;}}  seeMoreOnlineFormsIA () {if ( this.seeMoreOnlineFormIA ) {document.getElementById("lbl_onlineFormsIA_Inner2").style.display = 'none';
document.getElementById("lbl_onlineFormsIA_SeeMore").innerHTML = "See more";this.seeMoreOnlineFormIA = false;}else{document.getElementById("lbl_onlineFormsIA_Inner2").style.display = '';
document.getElementById("lbl_onlineFormsIA_SeeMore").innerHTML = "See less";this.seeMoreOnlineFormIA = true;}}        }
    