const fs = require('fs');

function compare(order, property) {
    let sortOrder = 1;
    if (order === "descend") {
        sortOrder = -1;
    }
    if (property) {
        return function (a, b) {
            let result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        };
    }
    else {
        return function (a, b) {
            let result = (a < b) ? -1 : (a > b) ? 1 : 0;
            return result * sortOrder;
        };
    }
}

class File {
    constructor (path, type) {
        
        this.path = null;
        this.name = null;
        this.nameWithoutExtension = null;
        this.extension = null;
        this.mimetype = null;
        this.data = null;
        this.size = null;

        if (path) {
            this.path = path;
            this.name = this._getName();
            this.nameWithoutExtension = this._getNameWithoutExtension();
            this.extension = this._getExtension();
            this.mimetype = type;
            try {
                this.data = fs.readFileSync(path);
            } catch (e) {
                this.data = null;
                console.log("Error reading file: " + e.message);
            }
            this.size = this._getSizeBytes();
        }
    }

    _getName () {
        return (this.path) ? this.path.replace(/\\+/g, "/").split('/').pop() : null;
    }

    _getNameWithoutExtension () {
        return (this.name && this.name.includes(".")) ? this.name.split(".")[0] : this.name;
    }

    _getExtension () {
        return (this.name && this.name.includes(".")) ? this.name.split(".").pop() : null;
    }

    _getSizeBytes () {
        let size = 0;
        try {
            size = fs.statSync(this.path).size;
        }
        catch(e) {
            size = 0;
        }
        return size;
    }
}

let getDirectoryFiles = function (path) {
    let files = [];

    if (path) {
        let fileNames = fs.readdirSync(path);

        for (let file of fileNames) {
            let newFile = new File(path + "/" + file, null);
            files.push(newFile);
        }
    }

    return files;
};

function getRandomIntInclusive (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function escapeRegExp (str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll (str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), "g"), replace);
}

// AM - Snippet added for stringToDate, no longer in use remove 01/09/2019
// function stringToDate (s, f = null) {
//     let d = Date.parse(s);
//     if (isNaN(d)) {
//         d = new Date(s * 1000);
//     }
//
//     return d;
// }
//}
function processFormObject(memberKey, memberValue, formDataList) {

    for (let key in memberValue) {
        //  create a new key that has the objectName[key], ignore functions/autoMapValues
        if (typeof memberValue[key] === "function" || key === "autoMapValues") {
            continue;
        }
        let newKey = `${memberKey}[${key}]`;
        addToFormList(newKey, memberValue[key], formDataList);
    }
}

function addToFormList(key, value, formDataList) {
    //  push into the list the key/value
    let braces = Array.isArray(value) ? "[]" : "";
    formDataList.push(`${key}${braces}=${encodeURIComponent(value)}`)
}

function encodeFormPostData(formData) {

    let formDataString = "";
    let formDataList = [];
    //  loop over the formData, this is an object with the key being the name
    for (let key in formData) {
        let value = formData[key];
        let type = typeof value;

        if (type === "object" && !Array.isArray(value)) {
            // as its an object we need to process it by looping over each of the members
            for (let memberKey in value) {
                let memberValue = value[memberKey];
                let memberType = typeof memberValue;
                if (memberType === "function" || memberKey === "autoMapValues") {
                    continue;
                }
                if (memberType === "object" && !Array.isArray(memberValue)) {
                    //  will need to loop over each of the objects as these are displayed as memberKey[ValueKey]=value
                    processFormObject(memberKey, memberValue, formDataList);
                }
                else {
                    addToFormList(memberKey, memberValue, formDataList);
                }
            }
        }
        else {
            //  push into the list the key/value
            addToFormList(key, value, formDataList);
        }
    }
    // join the key values with &
    formDataString = formDataList.join("&");

    return formDataString;
}

module.exports = {
    File: File,
    getDirectoryFiles: getDirectoryFiles,
    replaceAll: replaceAll,
    getRandomIntInclusive: getRandomIntInclusive,
    compare: compare,
    encodeFormPostData:encodeFormPostData
};