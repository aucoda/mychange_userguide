        //  _helpAndFAQsController definition",
        class _helpAndFAQsController  {
        constructor () {
        }
          load () {loadView("mychange_userguidUI", "helpAndFAQs");
this.addHeaderWidget();
this.addUserGuideMenu();
let userType = "";
if ( UserRole.indexOf("SSCL") !== -1 ) {document.getElementById("btn_helpAndFAQs9").style.display = 'block';}else if ( UserRole.indexOf("MPS") !== -1 ) {document.getElementById("btn_helpAndFAQs9").style.display = 'none';}}  addHeaderWidget () {headerWidget_remove();
small_headerWidget_remove();
let headerW = headerWidget_create("Change Guidance", "helpAndFAQsHeaderPlaceholder");
let small_headerW = small_headerWidget_create("Change Guidance", "small_helpAndFAQsHeaderPlaceholder");}  addUserGuideMenu () {userGuideMenuWidget_remove();
let userGuideMenuW = userGuideMenuWidget_create(true, "helpAndFAQsUserGuideMenuPlaceholder");
document.getElementById(userGuideMenuW.controlID("guidanceOptions_UGMW")).style.display = 'none';
document.getElementById(userGuideMenuW.controlID("helpAndFAQs_UGMW")).style.backgroundColor = "#E6E6E6";} async chooseHelpAndFAQsTopic (topicString) {this.unchooseHelpAndFAQsTopic();
if ( topicString === "Top questions" ) {document.getElementById("btn_helpAndFAQs1").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs1").style.color = "#1E87C3";}else if ( topicString === "RFC Lists & Notifications" ) {document.getElementById("btn_helpAndFAQs2").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs2").style.color = "#1E87C3";}else if ( topicString === "Raising an RFC" ) {document.getElementById("btn_helpAndFAQs3").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs3").style.color = "#1E87C3";}else if ( topicString === "Closed RFCs" ) {document.getElementById("btn_helpAndFAQs4").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs4").style.color = "#1E87C3";}else if ( topicString === "Consolidated IA and CAN forms" ) {document.getElementById("btn_helpAndFAQs5").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs5").style.color = "#1E87C3";}else if ( topicString === "RFC Management Screen" ) {document.getElementById("btn_helpAndFAQs6").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs6").style.color = "#1E87C3";}else if ( topicString === "MyChange Application" ) {document.getElementById("btn_helpAndFAQs7").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs7").style.color = "#1E87C3";}else if ( topicString === "User Accounts" ) {document.getElementById("btn_helpAndFAQs8").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs8").style.color = "#1E87C3";}else if ( topicString === "3rd Party Users" ) {document.getElementById("btn_helpAndFAQs9").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs9").style.color = "#1E87C3";}else if ( topicString === "GDPR" ) {document.getElementById("btn_helpAndFAQs10").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs10").style.color = "#1E87C3";}else if ( topicString === "Finance and Costs" ) {document.getElementById("btn_helpAndFAQs11").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs11").style.color = "#1E87C3";}else if ( topicString === "Acceptance of Implementation" ) {document.getElementById("btn_helpAndFAQs12").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs12").style.color = "#1E87C3";}else if ( topicString === "Dashboards" ) {document.getElementById("btn_helpAndFAQs13").style.border = "1px solid #1E87C3";
document.getElementById("btn_helpAndFAQs13").style.color = "#1E87C3";}await this.getHelpAndFAQs(topicString);}  unchooseHelpAndFAQsTopic () {document.getElementById("btn_helpAndFAQs1").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs1").style.color = "#000000";
document.getElementById("btn_helpAndFAQs2").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs2").style.color = "#000000";
document.getElementById("btn_helpAndFAQs3").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs3").style.color = "#000000";
document.getElementById("btn_helpAndFAQs4").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs4").style.color = "#000000";
document.getElementById("btn_helpAndFAQs5").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs5").style.color = "#000000";
document.getElementById("btn_helpAndFAQs6").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs6").style.color = "#000000";
document.getElementById("btn_helpAndFAQs7").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs7").style.color = "#000000";
document.getElementById("btn_helpAndFAQs8").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs8").style.color = "#000000";
document.getElementById("btn_helpAndFAQs9").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs9").style.color = "#000000";
document.getElementById("btn_helpAndFAQs10").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs10").style.color = "#000000";
document.getElementById("btn_helpAndFAQs11").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs11").style.color = "#000000";
document.getElementById("btn_helpAndFAQs12").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs12").style.color = "#000000";
document.getElementById("btn_helpAndFAQs13").style.border = "0px solid #000000";
document.getElementById("btn_helpAndFAQs13").style.color = "#000000";} async getHelpAndFAQs (faqTopic) {let FAQsList = [];
let userType = "";
if ( UserRole.indexOf("SSCL") !== -1 ) {userType = "SSCL";}else if ( UserRole.indexOf("MPS") !== -1 ) {userType = "MPS";}let  responseText_6sHYJTcN = await MyChangePortalWs.getHelpAndFAQsWf (userType,faqTopic);FAQsList = JSON.parse(responseText_6sHYJTcN);helpAndFAQsWidget_remove();
let count = 1;
if (FAQsList) {for (let __FAQsList_Idx = 0; __FAQsList_Idx < FAQsList.length; __FAQsList_Idx++) {this.addHelpAndFAQsWidget(FAQsList[__FAQsList_Idx].question,FAQsList[__FAQsList_Idx].answer,count);
count++;}}}  addHelpAndFAQsWidget (q,a,c) {let helpAndFAQsW = helpAndFAQsWidget_create(c, q, a, true, "helpAndFAQsPlaceholder");
document.getElementById(helpAndFAQsW.controlID("helpAndFAQsWidgetBody")).style.display = 'none';}        }
    