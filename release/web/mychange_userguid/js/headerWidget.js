let headerWidgetInstances = [];

let headerWidget_id = 0;

class headerWidget {
	constructor (headerName) {
		this.__id_ = "headerWidget_" + headerWidget_id++;
		this.deleted = false;
		this.parent = null;

		this.headerName = headerName;

		this.template = `
			<div id="${this.__id_}">
				<section id='headerWidgetWrapper_${this.__id_}' class='headerWidgetWrapper_widget_style' >
					<label id='lbl_headerWidget_${this.__id_}' class='lbl_headerWidget_widget_style' >${this.headerName}</label>
					<section id='userMenuPlaceholder_${this.__id_}' class='userMenuPlaceholder_widget_style' >

					</section>
				</section>
			</div>
		`;
	}

	bind (sectionId) {
		let element = document.getElementById(sectionId);
		if (element) {
			element.insertAdjacentHTML("beforeend", this.template);
			this.parent = document.getElementById(this.__id_);
		}
	}

	show () {
		if (!this.parent) {
			this.bind();
		}

		this.parent.classList.add("hidden");
	}

	hide () {
		if (this.parent) {
			this.parent.classList.add("hidden");
		}
	}

	controlID (controlName) {
		return `${controlName}_${this.__id_}`;
	}

	ID() {
		return this.__id_;
	}

	set_headerWidgetWrapper (value) {
		let element = document.getElementById(`headerWidgetWrapper_${this.__id_}`);
		if (element) {
			element.innerHTML = value;
		}
	}

	get_headerWidgetWrapper () {
		let element = document.getElementById(`headerWidgetWrapper_${this.__id_}`);
		if (element) {
			return element.innerHTML;
		}
		return null;
	}

}

function headerWidget_purgeDeleted() {
	headerWidgetInstances = headerWidgetInstances.filter(headerWidget => !headerWidget.deleted);
}

function headerWidget_remove(property = null, value = null) {
	let _headerWidget = headerWidget_all(property, value);
	if (!_headerWidget || _headerWidget.length === 0) {
		return null;
	}
	let removeEntries = property !== null && value !== null;
	_headerWidget.forEach(widgetItem => {
		if (!widgetItem.deleted) {
			if (removeEntries) {
				widgetItem.deleted = true;
			}

			let widgetItemContainer = document.getElementById(widgetItem.__id_);
			if (widgetItemContainer && widgetItemContainer.parentNode) {
				widgetItemContainer.parentNode.removeChild(widgetItemContainer);
			}
		}
	});

	if (!removeEntries) {
		headerWidgetInstances = [];
	}
}

function headerWidget_count(property, value) {
	//	take this opportunity to clean up
	headerWidget_purgeDeleted();
	let _headerWidget = headerWidgetInstances.filter(headerWidget => headerWidget[property] === value);
	return _headerWidget ? _headerWidget.length : 0;
}

function headerWidget_one(property, value) {
	//	take this opportunity to clean up
	headerWidget_purgeDeleted();
	let _headerWidget = headerWidgetInstances.filter(headerWidget => headerWidget[property] === value);
	return _headerWidget ? _headerWidget[0] : null;
}

function headerWidget_all(property, value) {
	//	take this opportunity to clean up
	headerWidget_purgeDeleted();
	let _headerWidget = property ? headerWidgetInstances.filter(headerWidget => headerWidget[property] === value): headerWidgetInstances;
	return _headerWidget ? _headerWidget : null;
}

function headerWidget_create (headerName, section) {
	let _headerWidget = new headerWidget(headerName);
	_headerWidget.bind(section);
	headerWidgetInstances.push(_headerWidget);
	return _headerWidget;
}

