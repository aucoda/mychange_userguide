//  Faq definition
    "use strict";
    const au_node = require('../lib/au_node');
    const FaqTopic = require('./FaqTopic');
const path = require('path');
    let globals = null;
    class Faq  {
        static setGlobals(_globals) {
            globals = _globals;
        }       
        constructor() {
            this.question = "";
this.answer = "";
this.role = "";
this.active = false;
this.faqTopic = new FaqTopic();
        }
        mapping (mapFaq) {if(mapFaq) {this.question = mapFaq.___data__ ? mapFaq.___data__.question : mapFaq.question;
this.answer = mapFaq.___data__ ? mapFaq.___data__.answer : mapFaq.answer;
this.role = mapFaq.___data__ ? mapFaq.___data__.role : mapFaq.role;
this.active = mapFaq.___data__ ? mapFaq.___data__.active : mapFaq.active;}}
    }
    module.exports = Faq;