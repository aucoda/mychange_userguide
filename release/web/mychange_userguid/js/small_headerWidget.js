let small_headerWidgetInstances = [];

let small_headerWidget_id = 0;

class small_headerWidget {
	constructor (headerName) {
		this.__id_ = "small_headerWidget_" + small_headerWidget_id++;
		this.deleted = false;
		this.parent = null;

		this.headerName = headerName;

		this.template = `
			<div id="${this.__id_}">
				<section id='small_headerWidgetWrapper_${this.__id_}' class='small_headerWidgetWrapper_widget_style' >
					<label id='lbl_small_headerWidget_${this.__id_}' class='lbl_small_headerWidget_widget_style' >${this.headerName}</label>
					<section id='small_userMenuPlaceholder_${this.__id_}' class='small_userMenuPlaceholder_widget_style' >

					</section>
				</section>
			</div>
		`;
	}

	bind (sectionId) {
		let element = document.getElementById(sectionId);
		if (element) {
			element.insertAdjacentHTML("beforeend", this.template);
			this.parent = document.getElementById(this.__id_);
		}
	}

	show () {
		if (!this.parent) {
			this.bind();
		}

		this.parent.classList.add("hidden");
	}

	hide () {
		if (this.parent) {
			this.parent.classList.add("hidden");
		}
	}

	controlID (controlName) {
		return `${controlName}_${this.__id_}`;
	}

	ID() {
		return this.__id_;
	}

	set_small_headerWidgetWrapper (value) {
		let element = document.getElementById(`small_headerWidgetWrapper_${this.__id_}`);
		if (element) {
			element.innerHTML = value;
		}
	}

	get_small_headerWidgetWrapper () {
		let element = document.getElementById(`small_headerWidgetWrapper_${this.__id_}`);
		if (element) {
			return element.innerHTML;
		}
		return null;
	}

}

function small_headerWidget_purgeDeleted() {
	small_headerWidgetInstances = small_headerWidgetInstances.filter(small_headerWidget => !small_headerWidget.deleted);
}

function small_headerWidget_remove(property = null, value = null) {
	let _small_headerWidget = small_headerWidget_all(property, value);
	if (!_small_headerWidget || _small_headerWidget.length === 0) {
		return null;
	}
	let removeEntries = property !== null && value !== null;
	_small_headerWidget.forEach(widgetItem => {
		if (!widgetItem.deleted) {
			if (removeEntries) {
				widgetItem.deleted = true;
			}

			let widgetItemContainer = document.getElementById(widgetItem.__id_);
			if (widgetItemContainer && widgetItemContainer.parentNode) {
				widgetItemContainer.parentNode.removeChild(widgetItemContainer);
			}
		}
	});

	if (!removeEntries) {
		small_headerWidgetInstances = [];
	}
}

function small_headerWidget_count(property, value) {
	//	take this opportunity to clean up
	small_headerWidget_purgeDeleted();
	let _small_headerWidget = small_headerWidgetInstances.filter(small_headerWidget => small_headerWidget[property] === value);
	return _small_headerWidget ? _small_headerWidget.length : 0;
}

function small_headerWidget_one(property, value) {
	//	take this opportunity to clean up
	small_headerWidget_purgeDeleted();
	let _small_headerWidget = small_headerWidgetInstances.filter(small_headerWidget => small_headerWidget[property] === value);
	return _small_headerWidget ? _small_headerWidget[0] : null;
}

function small_headerWidget_all(property, value) {
	//	take this opportunity to clean up
	small_headerWidget_purgeDeleted();
	let _small_headerWidget = property ? small_headerWidgetInstances.filter(small_headerWidget => small_headerWidget[property] === value): small_headerWidgetInstances;
	return _small_headerWidget ? _small_headerWidget : null;
}

function small_headerWidget_create (headerName, section) {
	let _small_headerWidget = new small_headerWidget(headerName);
	_small_headerWidget.bind(section);
	small_headerWidgetInstances.push(_small_headerWidget);
	return _small_headerWidget;
}

