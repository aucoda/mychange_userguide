/*
    dataServer
    Data Events support server
    Amit Yadav
    aucoda.com
    03/01/2018 Created
*/
"use strict";
let express = require("express"),
    bodyParser = require('body-parser');
let auSQLite = require("./auSQLite");
/* ---------------------EXPRESS WRAPPER-------------------------------- */
//  Start Express Server
//  start single threaded express sever running on a predefined port
let port = 3011;
let deferExit = false;
let triedToExit = false;
let MyChangePortalDs = require("./MyChangePortalDs");
let dataServer = express();
dataServer.use(bodyParser.json({
    limit: "100mb"
}));
dataServer.use(bodyParser.urlencoded({
    extended: true
}));
dataServer.get("/stopDataServer", [], function(req, res) {
    if (!deferExit) {
        res.json({
            message: "Stopping Data Server"
        });
        console.log("Exiting Data Server");
        process.exit();
    }
    else {
        res.json({
            message: "Data Server Busy"
        });
        console.log("Data Server Busy");
        triedToExit = true;
    }
});
//  Routes
// test route for checking if the server is up
dataServer.get("/ping", [], function(req, res) {
    res.json({
        success: true,
        message: "Data Server running on port " + port
    });
});
//  These routes can be called using the command line too
function validateDataFunction(parent, fn) {
    let msg = "";
    let success = true;
    if (!parent) {
        success = false;
        msg = "Unknown Data Table";
    }
    else if(fn.indexOf(".") !== -1){
        //check for a sub function, maybe a table function
        let subSplit = fn.split(".");
        let subVar = subSplit[0];
        let subFn =  subSplit[1];
        if(parent.hasOwnProperty(subVar)
            && parent[subVar].constructor.prototype.hasOwnProperty(subFn)
        ){
            // valid
        }else{
            success = false;
            msg = "Unknown Data Function " + fn;
        }
    }
    else if(!parent.constructor.prototype.hasOwnProperty(fn)) {
        // check if this is a direct function
        success = false;
        msg = "Unknown Data Function " + fn;
    }
    return {
        success: success,
        message: msg
    }
}
//  Stringify dates manually so that Shell received them in a sensible format
function __harmoniseDates__ (dataList) {
    for (let row of dataList) {
        for (let field in row) {
            if (row.hasOwnProperty(field) && Object.prototype.toString.call(row[field]) === "[object Date]") {
                try {
                    row[field] = row[field].toISOString().substr(0,10);
                }
                catch (e) {
                    row[field] = row[field].toString();
                }
            }
        }
    }
    return dataList;
}
//  Routes for console debug commands
//  table.command - x.all, x.one, x.update
//  datasource.function - x.companies
dataServer.post("/dataEvent", [], function(req, res) {
    let event = JSON.parse(req.body.event);
    auSQLite.setDataHome(req.body.home);
    let result = {
        success: true,
        message: "dataEventOK"
    };
    let validDataCall = false;
    switch(event.command) {
        case "all":
            if (event.data.table) {
                let pageSize = event.data.pageSize || 10;
                let pageNo = event.data.pageNo || 1;
                let limit =  event.data.limit || pageSize;
                let offset =  event.data.offset || ( (pageNo - 1) * pageSize );
                result = validateDataFunction(MyChangePortalDs[event.data.table], "all");
                if (result.success) {
                    validDataCall = true;
                    let orderBy = event.data.orderBy || ( event.data.table + "." + MyChangePortalDs[event.data.table].key); // default order by primary id field (ascending order by default)
                    // load results and send response via the callback
                    MyChangePortalDs[event.data.table].all(MyChangePortalDs, {
                             fields: null,
                             joins: null,
                             condition: null,
                             paged: limit,
                             offset: offset,
                             parameters: null,
                             orderby: orderBy
                         },
                        (_result) => {
                            result.message =  "SQL COMMAND : select all from " + event.data.table;
                            // fill the list of records and pass back
                            // for no records case a blank list will be passed
                            let list = __harmoniseDates__(MyChangePortalDs[event.data.table].getRows());
                            result.data = {
                                rows: list
                            };
                            res.json(result);
                        }, orderBy, limit, offset);
                }else {
                    res.json({
                        success: false,
                        message: "data command failed"
                    });
                }
            }else {
                res.json({
                    success: false,
                    message: "No data table"
                });
            }
            break;
         case "count":
            if (event.data.table) {
                validDataCall = true;
                MyChangePortalDs[event.data.table].count(null, null, (numOfRows) => {
                    let result = {
                        numOfRows: numOfRows
                    };
                    res.json(result);
                });
            } else {
                result = {
                    success: false,
                    message: "No data table"
                };
            }
            break;
        case "one":
            break;
        case "insert":
            break;
        case "_delete":
            break;
        case "deleteAll":
            if (event.data.table) {
                validDataCall = true;
                auWebsiteDs[event.data.table].deleteAll(() => {
                    res.json({success: true});
                });
            }
            else {
                result = {
                    success: false,
                    message: "No data table"
                };
            }
            break;
        case "update":
            break;
        case "function":
            if (event.data.fn === "setup" && event.data.ds !== "MyChangePortalDs") {
                // call this fn on the main parent
                let ds = event.data.ds;
                event.data.fn = ds + "." + event.data.fn;
                event.data.ds = "MyChangePortalDs";
            }
            if (event.data.ds === "MyChangePortalDs") {
                result = validateDataFunction(MyChangePortalDs, event.data.fn);
                if (result.success) {
                    if(event.data.fn.indexOf(".") === -1){
                        deferExit = true;
                        MyChangePortalDs[event.data.fn]((err) => {
                            deferExit = false;
                            if (triedToExit) {
                                console.log("Data Server Finished, Exiting");
                                process.exit();
                            }
                        });
                    } else {
                        // this is a subfn
                        let subSplit = event.data.fn.split(".");
                        let subVar = subSplit[0];
                        let subFn =  subSplit[1];
                        MyChangePortalDs[subVar][subFn]();
                    }
                }
            }
            else {
                result.success = false;
                result.message = "Unknown Data Source";
            }
            break;
        default:
            result.success = false;
            result.message = "Unknown Data Command";
            break;
    }
    if(!validDataCall){
        res.json(result);
    }
});
dataServer.listen(port);
console.log("Data Server Started on port : " + port);
