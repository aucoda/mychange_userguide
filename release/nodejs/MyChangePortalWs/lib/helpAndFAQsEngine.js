//  helpAndFAQsEngine definition
    "use strict";
    const au_node = require('../lib/au_node');
    const Faq = require('./Faq');const MyChangePortalDs = require('../../MyChangePortalDb/data/MyChangePortalDs');
const path = require('path');
    let globals = null;
    class helpAndFAQsEngine  {
        setGlobals(_globals) {
            globals = _globals;
        }
        constructor() {
        }
        async getHelpAndFAQs (faqRole,faqTopic) {
            let FAQsList = [];
await new Promise((resolve, reject) => { MyChangePortalDs.FaqTopicTable.one(MyChangePortalDs, {
        joins:['FaqTopicTable'],
        condition:`FaqTopicTable.topic = ?`,
        parameters:[faqTopic],
    }, () => resolve(true))})
;
let topicID = 0;
topicID = MyChangePortalDs.FaqTopicTable._ID();
await new Promise((resolve, reject) => { MyChangePortalDs.FaqTable.all(MyChangePortalDs, {
        joins:['FaqTable'],
        condition:`FaqTable.faqTopic_id = ?`,
        parameters:[topicID],
    }, () => resolve(true))})
;
let userType = "";
if ( faqRole.indexOf("SSCL") !== -1 ) {userType = "SSCL";}else if ( faqRole.indexOf("MPS") !== -1 ) {userType = "MPS";}while ( MyChangePortalDs.FaqTable.next() ) {let singleFAQ = new Faq();
singleFAQ.mapping(MyChangePortalDs.FaqTable.___data__);
if ( singleFAQ.role === userType || singleFAQ.role === "ALL" ) {FAQsList = FAQsList.concat(singleFAQ);}if ( userType === "SSCL" && faqTopic === "3rd Party Users" ) {if ( singleFAQ.role === "Third party user" ) {FAQsList = FAQsList.concat(singleFAQ);}}}
return FAQsList;
        }
    }
    module.exports = new helpAndFAQsEngine();
    