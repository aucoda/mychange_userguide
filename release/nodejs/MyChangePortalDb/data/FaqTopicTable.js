/*
    nodeJS sqlite class
    20/11/2017 Created
*/
"use strict";
let auData = require("./auDataSQL");
let au_node = require("../lib/au_node");
const FaqTopicTable_table_create = `
    CREATE TABLE FaqTopicTable (
        FaqTopicTable_id integer PRIMARY KEY,
        topic TEXT,		active INTEGER
    );
`;
const FaqTopicTable_table_drop = `
    DROP TABLE IF EXISTS FaqTopicTable;
`;
class FaqTopicTableColumns {
    constructor () {
        this.topic = "";this.active = false;
    }
    map(row) {
        this.topic = row["FaqTopicTable.topic"];this.active = Boolean(row["FaqTopicTable.active"]);
    }
    selectionFields(alias) {
        return `
            ${alias}.FaqTopicTable_id as [${alias}.FaqTopicTable_id], ${alias}.topic as [${alias}.topic], ${alias}.active as [${alias}.active]
        `;
    }
}
class FaqTopicTable extends auData {
    constructor (database) {
        super(database);
        this.cursor = null;
        this.name = "FaqTopicTable";
        this.key = "FaqTopicTable_id";
        //  fields
        this.___data__ = new FaqTopicTableColumns();
        //  lists
        this.fields = [
            "topic","active"
        ];
        //  join clauses
        this.joinClauses = {
        };
    }
    setup () {
        this.cache(FaqTopicTable_table_drop);
        this.cache(FaqTopicTable_table_create);
    }
    map(row) {
        if (row) {
            this.FaqTopicTable_id = row["FaqTopicTable.FaqTopicTable_id"];
            this.___data__.map(row);
            this.___data__.FaqTopicTable_id = row["FaqTopicTable.FaqTopicTable_id"];
        }
    }
    reset() {
        this.FaqTopicTable_id = 0;
        // reset the __data_ as this could still contain data from previous call
        this.___data__ = new FaqTopicTableColumns();
    }
    parameterise(_object) {
        return [_object.topic, _object.active];
    }
    update(parameters, callback) {
        let values = [];
        let condition = parameters.condition.join(" AND ");
        let usedFields = parameters.fields;
        values = values.concat(parameters.fieldParameters).concat(parameters.conditionParameters);
        super.update(usedFields, values, condition, callback);
    }
    add(parameters, callback) {
        super.insert(parameters.fields, parameters.fieldParameters, callback);
    }
    updateEntity(newRecord, dataid, callback) {
        let values = [];
        let usedFields = [];
        let row = newRecord;
        if (newRecord.hasOwnProperty("___data__")) {
            row = newRecord.___data__;
        }
        for (let field in row) {
            if (field !== this.key && row.hasOwnProperty(field)
                && field !== dataid
                && (typeof row[field] !== "object" || Object.prototype.toString.call(row[field]) === "[object Date]" || (row[field] && row[field].data))
                && this.fields.indexOf(field) >= 0
            ) {
                usedFields.push(field);
                values.push(row[field]);
            }
        }
        if (newRecord.hasOwnProperty(dataid)) {
            let condition = this.key + " = " + newRecord[dataid];
            super.updateEntity(usedFields, values, condition, callback);
        }
        else {
            console.log ("FaqTopicTable - Cannot update record, no dataid");
        }
    }
    addEntity(newRecord, callback) {
        let values = [];
        let usedFields = [];
        let row = newRecord;
        if (newRecord.hasOwnProperty("___data__")) {
            row = newRecord.___data__;
        }
        for (let field in row) {
            if (field !== this.key && row.hasOwnProperty(field)
                && (typeof row[field] !== "object" || Object.prototype.toString.call(row[field]) === "[object Date]" || (row[field] && row[field].data))
                && this.fields.indexOf(field) >= 0
            ) {
                usedFields.push(field);
                values.push(row[field]);
            }
        }
        super.insert(usedFields, values, callback);
    }
    _delete(parameters, callback) {
            let condition = null;
            let params = null;
            if(parameters) {
                condition = Array.isArray(parameters.condition) ? parameters.condition.join(" AND ") : parameters.condition;
                params = parameters.parameters;
            }
        super._delete(condition, params, (changes) => {
            if (callback) {
                callback(changes);
            }
        });
    }
    delete(parameters, callback) {
            let condition = null;
            let params = null;
            if(parameters) {
                condition = Array.isArray(parameters.condition) ? parameters.condition.join(" AND ") : parameters.condition;
                params = parameters.parameters;
            }
        super._delete(condition, params, (changes) => {
            if (callback) {
                callback(changes);
            }
        });
    }
    deleteAll(callback) {
        super.deleteAll((changes) => {
            if (callback) {
                callback(changes);
            }
        });
    }
    //  select data
    one(MyChangePortalDs, parameters, callback) {
        this.all(MyChangePortalDs, parameters, (cursor) => {
            if (this.cursor && this.cursor.valid) {
                this.cursor.next();
            }
            if(this.cursor.row) {
                this.map(this.cursor.row);
            }
            if (callback) {
                callback(this);
            }
        });
    }
    _ID() {
        return this.FaqTopicTable_id;
    }
    count(MyChangePortalDs, parameters, callback) {
            let joins = null;
            let condition = null;
            let params = null;
            if(parameters) {
                joins = parameters.joins;
                condition = parameters.condition;
                params = parameters.parameters;
            }
            if (!joins) {
                joins = [];
            }
        super.count(MyChangePortalDs, condition, params, joins, callback);
    }
    all(MyChangePortalDs, parameters, callback) {
        //  build field sets
        let fields = parameters.fields;
        let joins = parameters.joins;
        //  no fields passed in, so build the full field set
        if (!fields) {
            // add in the parent table
            joins = ["FaqTopicTable"];
            fields = MyChangePortalDs.buildFieldSet([{table: "FaqTopicTable", alias:"FaqTopicTable"}]);
        }
        if (parameters.paged) {
            this.limit = parameters.paged;
            if(parameters.offset !== null){
                this.offset = parameters.offset;
            }
            //  set a default orderby
            if (!parameters.orderby) {
                parameters.orderby = "FaqTopicTable.FaqTopicTable_id";
            }
        }
        else {
            this.limit = null;
            this.offset = 0;
        }
        this.reset();
        this.select(MyChangePortalDs, fields, parameters.condition, parameters.parameters, joins, parameters.orderby, parameters.parentJoin, callback);
    }
    select(MyChangePortalDs, fields, condition, parameters, joins, orderBy, parentJoin, callback) {
        super.select(MyChangePortalDs, fields, condition, parameters, joins, orderBy, parentJoin,
            (cursor) => {
                this.cursor = cursor;
                if (callback) {
                    callback(this);
                }
        });
    }
    next() {
        this.cursor.next();
        if(this.cursor.row && this.cursor.row.hasOwnProperty("FaqTopicTable.FaqTopicTable_id") && this.cursor.row["FaqTopicTable.FaqTopicTable_id"]){
            if (this.cursor.valid) {
                this.map(this.cursor.row);
                return this;
            }
        }
        this.FaqTopicTable_id = null;
        return null;
    }
    valid() {
        if(this.cursor.valid) {
            return this;
        }
        return null;
    }
    nextChild() {
        this.cursor.next();
        if (this.cursor.valid && this.cursor.row["FaqTopicTable.FaqTopicTable_id"] === this.FaqTopicTable_id) {
            this.map(this.cursor.row);
            return this;
        }
        return null;
    }
    getRows(){
        let resultSet = [];
        let rowIndex = 0;
        if (this.cursor && this.cursor.valid) {
             while (this.next()) {
                let record = {};
                record.FaqTopicTable_id = this.___data__.FaqTopicTable_id;
                record.topic = this.___data__.topic;record.active = this.___data__.active;
                resultSet.push(record);
                rowIndex++;
            }
        }
        return resultSet;
    }
    getPage(pageSize = 10, pageNo = 1) {
        let resultSet = [];
        let rowIndex = 0;
        let skipRows = ( pageNo - 1 ) * pageSize;
        if (this.cursor.valid) {
            do {
                rowIndex++;
                if(rowIndex <= skipRows){
                    continue;
                }
                let record = {};
                resultSet.push(record);
            } while (this.next() && resultSet.length < pageSize)
        }
        return resultSet;
    }
}
module.exports = FaqTopicTable;