/*
    auSQLite.js
    
    SQLITE3 NodeJS support
    
    Raj Curwen
    aucoda.com
    
    21/11/2017 Created
*/

"use strict";

//  require sqlite - verbose mode to start
const sqlite3 = require('sqlite3').verbose();
let path = require('path');

// global data home
let _dataHome =  null;

function setDebugDataHome(){
    let currentDir = __dirname;
    // eg : \shell-homes\amit\_debug\suppliermanagement\nodejs\SupplierManagementDb\data
    let appDir = path.resolve(path.join(currentDir, "../../../../"));
    let appName = path.basename(appDir);
    let relativePath = path.resolve(path.join(appDir, "../../_db"));
    _dataHome = path.join(relativePath, appName);
}

//  cached sql statements - save these using cache(sql), execute using commitSQLCache
let sqlCache = [];

//  ----------------------
//  AU support - sqlite db
class auSQLite {
    constructor(database) {
        this.db = null;
        this.cursor = null;
        // this can be a full path for the db.
        // if not path is there use in memory db
        this._database = database;
    }

    commitSQLCache(callback) {
        this.open();
        this.db.serialize(() => {
            for (let i=0; i < sqlCache.length; i++) {
                let sql = sqlCache[i];
                this.db.run(sql, [], (err) => {
                    if (err) {
                        console.log(err);
                    }

                    if (i === sqlCache.length-1) {
                        if (callback) {
                            callback();
                        }

                        this.close();
                    }
                });
            }
        });
    }

    cache (sql) {
        sqlCache.push(sql);
    }

    open() {
        if(_dataHome === null){
            setDebugDataHome();
        }

        let database = _dataHome? path.join(_dataHome, this._database) : ":memory:";
        this.db = new sqlite3.Database(database, (err) => {
            if (err) {
                return console.error(err.message);
            }
        });
    }

    close() {
        this.db.close((err) => {
            if (err) {
                return console.error(err.message);
            }
        });
    }

    insert(sql, parameters, callback) {
        this.open();
        if (this.db) {
            this.db.serialize(() => {
                this.db.run(sql, parameters, function(err) {
                    if (err) {
                        return console.log(err.message);
                    }

                    // get the last insert id
                    if (callback) {
                        callback(this.lastID);
                    }
                });

            });

            //  use basic execSQL it is faster - but more overhead to maintain
            //  AU can handle the complexity - it mirrors C# Exec
            //  this.db.execSQL(`INSERT INTO ${table} (${fields}) VALUES (?)`, values);

            this.close();
        }
    }

    update(sql, parameters, callback) {
        this.open();
        this.db.serialize(() => {
            this.db.run(sql, parameters, function(err) {
                if (err) {
                    return console.error(err.message);
                }

                if (callback) {
                    callback(this.changes);
                }
            });
        });

        this.close();
    }

    _delete(sql, parameters, callback) {
        this.open();
        this.db.serialize(() => {
            this.db.run(sql, parameters, function (err) {
                if (err) {
                    return console.error(err.message);
                }

                if (callback) {
                    callback(this.changes);
                }
            });
        });

        this.close();
    }

    deleteAll(sql, callback) {
        this.open();
        this.db.serialize(() => {
            this.db.run(sql, [], function (err) {
                if (err) {
                    return console.error(err.message);
                }

                if (callback) {
                    callback(this.changes);
                }
            });
        });

        this.close();
    }

    select(sql, parameters, callback) {
        this.open();
        if(parameters === undefined || parameters === null) {
            parameters = [];
        }

        this.db.serialize(() => {
            this.db.all(sql, parameters, (err, rows) => {
                if (err) {
                    throw err;
                }

                if (callback) {
                    callback(rows);
                }
            });
        });

        this.close();
    }

    tables() {
        this.open();
        this.db.serialize(() => {
            let sql = "select name from sqlite_master where type='table'";

            this.db.all(sql, [], (err, tables) => {
                console.log("Tables");
                console.log(tables);
            });
        });

        this.close();
    }
}

module.exports = {
    create : (database)=> {return new auSQLite(database);},
    setDataHome: (dataHome)=> { _dataHome = dataHome; }
};