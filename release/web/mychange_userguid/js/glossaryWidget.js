let glossaryWidgetInstances = [];

let glossaryWidget_id = 0;

class glossaryWidget {
	constructor (term, explanation) {
		this.__id_ = "glossaryWidget_" + glossaryWidget_id++;
		this.deleted = false;
		this.parent = null;

		this.term = term;
		this.explanation = explanation;

		this.template = `
			<div id="${this.__id_}">
				<section id='glossaryWidgetWrapper_${this.__id_}' class='glossaryWidgetWrapper_widget_style' >
					<label id='lbl_glossaryWidget_1_${this.__id_}' class='lbl_glossaryWidget_1_widget_style' >${this.term}</label>
					<label id='lbl_glossaryWidget_2_${this.__id_}' class='lbl_glossaryWidget_2_widget_style' >${this.explanation}</label>
				</section>
			</div>
		`;
	}

	bind (sectionId) {
		let element = document.getElementById(sectionId);
		if (element) {
			element.insertAdjacentHTML("beforeend", this.template);
			this.parent = document.getElementById(this.__id_);
		}
	}

	show () {
		if (!this.parent) {
			this.bind();
		}

		this.parent.classList.add("hidden");
	}

	hide () {
		if (this.parent) {
			this.parent.classList.add("hidden");
		}
	}

	controlID (controlName) {
		return `${controlName}_${this.__id_}`;
	}

	ID() {
		return this.__id_;
	}

	set_glossaryWidgetWrapper (value) {
		let element = document.getElementById(`glossaryWidgetWrapper_${this.__id_}`);
		if (element) {
			element.innerHTML = value;
		}
	}

	get_glossaryWidgetWrapper () {
		let element = document.getElementById(`glossaryWidgetWrapper_${this.__id_}`);
		if (element) {
			return element.innerHTML;
		}
		return null;
	}

}

function glossaryWidget_purgeDeleted() {
	glossaryWidgetInstances = glossaryWidgetInstances.filter(glossaryWidget => !glossaryWidget.deleted);
}

function glossaryWidget_remove(property = null, value = null) {
	let _glossaryWidget = glossaryWidget_all(property, value);
	if (!_glossaryWidget || _glossaryWidget.length === 0) {
		return null;
	}
	let removeEntries = property !== null && value !== null;
	_glossaryWidget.forEach(widgetItem => {
		if (!widgetItem.deleted) {
			if (removeEntries) {
				widgetItem.deleted = true;
			}

			let widgetItemContainer = document.getElementById(widgetItem.__id_);
			if (widgetItemContainer && widgetItemContainer.parentNode) {
				widgetItemContainer.parentNode.removeChild(widgetItemContainer);
			}
		}
	});

	if (!removeEntries) {
		glossaryWidgetInstances = [];
	}
}

function glossaryWidget_count(property, value) {
	//	take this opportunity to clean up
	glossaryWidget_purgeDeleted();
	let _glossaryWidget = glossaryWidgetInstances.filter(glossaryWidget => glossaryWidget[property] === value);
	return _glossaryWidget ? _glossaryWidget.length : 0;
}

function glossaryWidget_one(property, value) {
	//	take this opportunity to clean up
	glossaryWidget_purgeDeleted();
	let _glossaryWidget = glossaryWidgetInstances.filter(glossaryWidget => glossaryWidget[property] === value);
	return _glossaryWidget ? _glossaryWidget[0] : null;
}

function glossaryWidget_all(property, value) {
	//	take this opportunity to clean up
	glossaryWidget_purgeDeleted();
	let _glossaryWidget = property ? glossaryWidgetInstances.filter(glossaryWidget => glossaryWidget[property] === value): glossaryWidgetInstances;
	return _glossaryWidget ? _glossaryWidget : null;
}

function glossaryWidget_create (term, explanation, section) {
	let _glossaryWidget = new glossaryWidget(term, explanation);
	_glossaryWidget.bind(section);
	glossaryWidgetInstances.push(_glossaryWidget);
	return _glossaryWidget;
}

