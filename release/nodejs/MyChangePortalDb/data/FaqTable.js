/*
    nodeJS sqlite class
    20/11/2017 Created
*/
"use strict";
let auData = require("./auDataSQL");
let au_node = require("../lib/au_node");
const FaqTable_table_create = `
    CREATE TABLE FaqTable (
        FaqTable_id integer PRIMARY KEY,
        question TEXT,		answer TEXT,		role TEXT,		active INTEGER,		faqTopic_id INTEGER,
        FOREIGN KEY (faqTopic_id) REFERENCES FaqTopicTable (FaqTopicTable_id)	ON DELETE CASCADE ON UPDATE NO ACTION
    );
`;
const FaqTable_table_drop = `
    DROP TABLE IF EXISTS FaqTable;
`;
class FaqTableColumns {
    constructor () {
        this.question = "";this.answer = "";this.role = "";this.active = false;this.faqTopic_id = 0;
    }
    map(row) {
        this.question = row["FaqTable.question"];this.answer = row["FaqTable.answer"];this.role = row["FaqTable.role"];this.active = Boolean(row["FaqTable.active"]);//	FaqTopicTablethis.faqTopic = {}; this.faqTopic.topic = row["FaqTopicTable_faqTopic.topic"];this.faqTopic.active = Boolean(row["FaqTopicTable_faqTopic.active"]);this.faqTopic_id = row["FaqTable.faqTopic_id"];
    }
    selectionFields(alias) {
        return `
            ${alias}.FaqTable_id as [${alias}.FaqTable_id], ${alias}.question as [${alias}.question], ${alias}.answer as [${alias}.answer], ${alias}.role as [${alias}.role], ${alias}.active as [${alias}.active], ${alias}.faqTopic_id as [${alias}.faqTopic_id]
        `;
    }
}
class FaqTable extends auData {
    constructor (database) {
        super(database);
        this.cursor = null;
        this.name = "FaqTable";
        this.key = "FaqTable_id";
        //  fields
        this.___data__ = new FaqTableColumns();
        //  lists
        this.fields = [
            "question","answer","role","active","faqTopic_id"
        ];
        //  join clauses
        this.joinClauses = {
            FaqTopicTable_faqTopic: 'LEFT JOIN FaqTopicTable FaqTopicTable_faqTopic ON FaqTopicTable_faqTopic.FaqTopicTable_id = FaqTable.faqTopic_id'
        };
    }
    setup () {
        this.cache(FaqTable_table_drop);
        this.cache(FaqTable_table_create);
    }
    map(row) {
        if (row) {
            this.FaqTable_id = row["FaqTable.FaqTable_id"];
            this.___data__.map(row);
            this.___data__.FaqTable_id = row["FaqTable.FaqTable_id"];
        }
    }
    reset() {
        this.FaqTable_id = 0;
        // reset the __data_ as this could still contain data from previous call
        this.___data__ = new FaqTableColumns();
    }
    parameterise(_object) {
        return [_object.question, _object.answer, _object.role, _object.active, _object.faqTopic_id];
    }
    update(parameters, callback) {
        let values = [];
        let condition = parameters.condition.join(" AND ");
        let usedFields = parameters.fields;
        values = values.concat(parameters.fieldParameters).concat(parameters.conditionParameters);
        super.update(usedFields, values, condition, callback);
    }
    add(parameters, callback) {
        super.insert(parameters.fields, parameters.fieldParameters, callback);
    }
    updateEntity(newRecord, dataid, callback) {
        let values = [];
        let usedFields = [];
        let row = newRecord;
        if (newRecord.hasOwnProperty("___data__")) {
            row = newRecord.___data__;
        }
        for (let field in row) {
            if (field !== this.key && row.hasOwnProperty(field)
                && field !== dataid
                && (typeof row[field] !== "object" || Object.prototype.toString.call(row[field]) === "[object Date]" || (row[field] && row[field].data))
                && this.fields.indexOf(field) >= 0
            ) {
                usedFields.push(field);
                values.push(row[field]);
            }
        }
        if (newRecord.hasOwnProperty(dataid)) {
            let condition = this.key + " = " + newRecord[dataid];
            super.updateEntity(usedFields, values, condition, callback);
        }
        else {
            console.log ("FaqTable - Cannot update record, no dataid");
        }
    }
    addEntity(newRecord, callback) {
        let values = [];
        let usedFields = [];
        let row = newRecord;
        if (newRecord.hasOwnProperty("___data__")) {
            row = newRecord.___data__;
        }
        for (let field in row) {
            if (field !== this.key && row.hasOwnProperty(field)
                && (typeof row[field] !== "object" || Object.prototype.toString.call(row[field]) === "[object Date]" || (row[field] && row[field].data))
                && this.fields.indexOf(field) >= 0
            ) {
                usedFields.push(field);
                values.push(row[field]);
            }
        }
        super.insert(usedFields, values, callback);
    }
    _delete(parameters, callback) {
            let condition = null;
            let params = null;
            if(parameters) {
                condition = Array.isArray(parameters.condition) ? parameters.condition.join(" AND ") : parameters.condition;
                params = parameters.parameters;
            }
        super._delete(condition, params, (changes) => {
            if (callback) {
                callback(changes);
            }
        });
    }
    delete(parameters, callback) {
            let condition = null;
            let params = null;
            if(parameters) {
                condition = Array.isArray(parameters.condition) ? parameters.condition.join(" AND ") : parameters.condition;
                params = parameters.parameters;
            }
        super._delete(condition, params, (changes) => {
            if (callback) {
                callback(changes);
            }
        });
    }
    deleteAll(callback) {
        super.deleteAll((changes) => {
            if (callback) {
                callback(changes);
            }
        });
    }
    //  select data
    one(MyChangePortalDs, parameters, callback) {
        this.all(MyChangePortalDs, parameters, (cursor) => {
            if (this.cursor && this.cursor.valid) {
                this.cursor.next();
            }
            if(this.cursor.row) {
                this.map(this.cursor.row);
            }
            if (callback) {
                callback(this);
            }
        });
    }
    _ID() {
        return this.FaqTable_id;
    }
    count(MyChangePortalDs, parameters, callback) {
            let joins = null;
            let condition = null;
            let params = null;
            if(parameters) {
                joins = parameters.joins;
                condition = parameters.condition;
                params = parameters.parameters;
            }
            if (!joins) {
                joins = [{table:"FaqTopicTable", alias:"FaqTopicTable_faqTopic"}];
            }
        super.count(MyChangePortalDs, condition, params, joins, callback);
    }
    all(MyChangePortalDs, parameters, callback) {
        //  build field sets
        let fields = parameters.fields;
        let joins = parameters.joins;
        //  no fields passed in, so build the full field set
        if (!fields) {
            // add in the parent table
            joins = ["FaqTable", "FaqTopicTable_faqTopic"];
            fields = MyChangePortalDs.buildFieldSet([{table: "FaqTable", alias:"FaqTable"}, {table:"FaqTopicTable", alias:"FaqTopicTable_faqTopic"}]);
        }
        if (parameters.paged) {
            this.limit = parameters.paged;
            if(parameters.offset !== null){
                this.offset = parameters.offset;
            }
            //  set a default orderby
            if (!parameters.orderby) {
                parameters.orderby = "FaqTable.FaqTable_id";
            }
        }
        else {
            this.limit = null;
            this.offset = 0;
        }
        this.reset();
        this.select(MyChangePortalDs, fields, parameters.condition, parameters.parameters, joins, parameters.orderby, parameters.parentJoin, callback);
    }
    select(MyChangePortalDs, fields, condition, parameters, joins, orderBy, parentJoin, callback) {
        super.select(MyChangePortalDs, fields, condition, parameters, joins, orderBy, parentJoin,
            (cursor) => {
                this.cursor = cursor;
                if (callback) {
                    callback(this);
                }
        });
    }
    next() {
        this.cursor.next();
        if(this.cursor.row && this.cursor.row.hasOwnProperty("FaqTable.FaqTable_id") && this.cursor.row["FaqTable.FaqTable_id"]){
            if (this.cursor.valid) {
                this.map(this.cursor.row);
                return this;
            }
        }
        this.FaqTable_id = null;
        return null;
    }
    valid() {
        if(this.cursor.valid) {
            return this;
        }
        return null;
    }
    nextChild() {
        this.cursor.next();
        if (this.cursor.valid && this.cursor.row["FaqTable.FaqTable_id"] === this.FaqTable_id) {
            this.map(this.cursor.row);
            return this;
        }
        return null;
    }
    getRows(){
        let resultSet = [];
        let rowIndex = 0;
        if (this.cursor && this.cursor.valid) {
             while (this.next()) {
                let record = {};
                record.FaqTable_id = this.___data__.FaqTable_id;
                record.question = this.___data__.question;record.answer = this.___data__.answer;record.role = this.___data__.role;record.active = this.___data__.active;record.faqTopic_id = this.___data__.faqTopic_id;
                resultSet.push(record);
                rowIndex++;
            }
        }
        return resultSet;
    }
    getPage(pageSize = 10, pageNo = 1) {
        let resultSet = [];
        let rowIndex = 0;
        let skipRows = ( pageNo - 1 ) * pageSize;
        if (this.cursor.valid) {
            do {
                rowIndex++;
                if(rowIndex <= skipRows){
                    continue;
                }
                let record = {};
                resultSet.push(record);
            } while (this.next() && resultSet.length < pageSize)
        }
        return resultSet;
    }
}
module.exports = FaqTable;