/*
    nodeJS datasource class
    30/11/2017 Created
*/
"use strict";
let path = require("path");
const FaqTable = require('./FaqTable.js'),FaqTopicTable = require('./FaqTopicTable.js');let auDataSQL = require("./auDataSQL");
class MyChangePortalDs {
    constructor() {
        this.FaqTable = new FaqTable("MyChangePortalDs.db");this.FaqTopicTable = new FaqTopicTable("MyChangePortalDs.db");
    }
    setup(callback) {
        let aud = new auDataSQL("MyChangePortalDs");
        this.FaqTable.setup();this.FaqTopicTable.setup();
        aud.commitSQLCache(callback);
    }
    buildFieldSet (tableList) {
        let fieldSet = [];
        for (let i = 0; i < tableList.length; i++) {
            let table = tableList[i];
            if (this[table.table]) {
                fieldSet.push(this[table.table].___data__.selectionFields(table.alias));
            }
        }
        return fieldSet.join(", ");
    }
    //  data functions
}
module.exports = new MyChangePortalDs();
