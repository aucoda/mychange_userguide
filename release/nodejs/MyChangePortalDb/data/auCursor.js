/*
    auCursor
    
    cursor support
    
    Raj Curwen
    aucoda.com
    
    21/11/2017 Created
*/

"use strict";

module.exports = class auCursor {
    constructor(rows) {
        this.row = null;
        this.rows = rows;
        this.at = 0;
        this.valid = rows !== null && rows.length;
    }

    next() {

        if (this.at < this.rows.length) {
            this.row = this.rows[this.at];
            this.valid = true;
            this.at++;
            return this.row;
        }
        this.row = null;
        this.valid = false;
        return null;
    }
};