let userGuideMenuWidgetInstances = [];

let userGuideMenuWidget_id = 0;

class userGuideMenuWidget {
	constructor (optionsHidden) {
		this.__id_ = "userGuideMenuWidget_" + userGuideMenuWidget_id++;
		this.deleted = false;
		this.parent = null;

		this.optionsHidden = optionsHidden;

		this.template = `
			<div id="${this.__id_}">
				<section id='userGuideMenuWidgetWrapper_${this.__id_}' class='userGuideMenuWidgetWrapper_widget_style' >
					<section id='changeProcess_UGMW_${this.__id_}' class='changeProcess_UGMW_widget_style' onclick='event.stopPropagation(); chooseMenuItem(\"Change Process\");'>
						<img id='img_changeProcess_UGMW_${this.__id_}' src='./Images/folder_black.svg' alt='' title='' class='img_changeProcess_UGMW_widget_style'/>
						<label id='lbl_changeProcess_UGMW_${this.__id_}' class='lbl_changeProcess_UGMW_widget_style' >Change Process</label>
					</section>
					<section id='changeProcessTailoring_UGMW_${this.__id_}' class='changeProcessTailoring_UGMW_widget_style' onclick='event.stopPropagation(); chooseMenuItem(\"Change Process Tailoring\");'>
						<img id='img_changeProcessTailoring_UGMW_${this.__id_}' src='./Images/folder_black.svg' alt='' title='' class='img_changeProcessTailoring_UGMW_widget_style'/>
						<label id='lbl_changeProcessTailoring_UGMW_${this.__id_}' class='lbl_changeProcessTailoring_UGMW_widget_style' >Change Process Tailoring</label>
					</section>
					<section id='myChangeOverview_UGMW_${this.__id_}' class='myChangeOverview_UGMW_widget_style' onclick='event.stopPropagation(); chooseMenuItem(\"MyChange Overview\");'>
						<img id='img_myChangeOverview_UGMW_${this.__id_}' src='./Images/folder_black.svg' alt='' title='' class='img_myChangeOverview_UGMW_widget_style'/>
						<label id='lbl_myChangeOverview_UGMW_${this.__id_}' class='lbl_myChangeOverview_UGMW_widget_style' >MyChange Overview</label>
					</section>
					<section id='rolesAndResponsibilities_UGMW_${this.__id_}' class='rolesAndResponsibilities_UGMW_widget_style' onclick='event.stopPropagation(); chooseMenuItem(\"Roles & Responsibilities\");'>
						<img id='img_rolesAndResponsibilitiesOverlay_UGMW_${this.__id_}' src='./Images/folder_black.svg' alt='' title='' class='img_rolesAndResponsibilitiesOverlay_UGMW_widget_style'/>
						<label id='lbl_rolesAndResponsibilities_UGMW_${this.__id_}' class='lbl_rolesAndResponsibilities_UGMW_widget_style' >Roles & Responsibilities</label>
					</section>
					<section id='helpAndFAQs_UGMW_${this.__id_}' class='helpAndFAQs_UGMW_widget_style' onclick='event.stopPropagation(); chooseMenuItem(\"Help & FAQs\");'>
						<img id='img_helpAndFAQs_UGMW_${this.__id_}' src='./Images/folder_black.svg' alt='' title='' class='img_helpAndFAQs_UGMW_widget_style'/>
						<label id='lbl_helpAndFAQs_UGMW_${this.__id_}' class='lbl_helpAndFAQs_UGMW_widget_style' >Help & FAQs</label>
					</section>
					<section id='glossary_UGMW_${this.__id_}' class='glossary_UGMW_widget_style' onclick='event.stopPropagation(); chooseMenuItem(\"Glossary\");'>
						<img id='img_glossary_UGMW_${this.__id_}' src='./Images/folder_black.svg' alt='' title='' class='img_glossary_UGMW_widget_style'/>
						<label id='lbl_glossary_UGMW_${this.__id_}' class='lbl_glossary_UGMW_widget_style' >Glossary</label>
					</section>
					<section id='guidance_UGMW_${this.__id_}' class='guidance_UGMW_widget_style' onclick='event.stopPropagation(); chooseMenuItem(\"Guidance\");'>
						<img id='img_guidance_UGMW_${this.__id_}' src='./Images/folder_black.svg' alt='' title='' class='img_guidance_UGMW_widget_style'/>
						<label id='lbl_guidance_UGMW_${this.__id_}' class='lbl_guidance_UGMW_widget_style' >Guidance</label>
						<img id='img_guidanceArrow_UGMW_${this.__id_}' src='./Images/downArrow_black.svg' alt='' title='' class='img_guidanceArrow_UGMW_widget_style'/>
					</section>
					<section id='guidanceOptions_UGMW_${this.__id_}' class='guidanceOptions_UGMW_widget_style' >
						<label id='lbl_guidanceOption1_${this.__id_}' class='lbl_guidanceOption1_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(1,true);'>RFC Raised</label>
						<label id='lbl_guidanceOption2_${this.__id_}' class='lbl_guidanceOption2_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(2,true);'>RFC Approval</label>
						<label id='lbl_guidanceOption3_${this.__id_}' class='lbl_guidanceOption3_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(3,true);'>SSCL Triage</label>
						<label id='lbl_guidanceOption4_${this.__id_}' class='lbl_guidanceOption4_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(4,true);'>Early Engagement</label>
						<label id='lbl_guidanceOption5_${this.__id_}' class='lbl_guidanceOption5_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(5,true);'>3rd Party Impacting</label>
						<label id='lbl_guidanceOption6_${this.__id_}' class='lbl_guidanceOption6_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(6,true);'>SSCL Impacting</label>
						<label id='lbl_guidanceOption7_${this.__id_}' class='lbl_guidanceOption7_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(7,true);'>Client Approval</label>
						<label id='lbl_guidanceOption8_${this.__id_}' class='lbl_guidanceOption8_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(8,true);'>Scheduling</label>
						<label id='lbl_guidanceOption9_${this.__id_}' class='lbl_guidanceOption9_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(9,true);'>In Delivery</label>
						<label id='lbl_guidanceOption10_${this.__id_}' class='lbl_guidanceOption10_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(10,true);'>AOI & Finalising</label>
						<label id='lbl_guidanceOption11_${this.__id_}' class='lbl_guidanceOption11_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(11,true);'>Complete / Closed</label>
						<label id='lbl_guidanceOption12_${this.__id_}' class='lbl_guidanceOption12_widget_style' onclick='event.stopPropagation(); guidanceController.chooseGuidanceItem(12,true);'>Query</label>
					</section>
				</section>
			</div>
		`;
	}

	bind (sectionId) {
		let element = document.getElementById(sectionId);
		if (element) {
			element.insertAdjacentHTML("beforeend", this.template);
			this.parent = document.getElementById(this.__id_);
		}
	}

	show () {
		if (!this.parent) {
			this.bind();
		}

		this.parent.classList.add("hidden");
	}

	hide () {
		if (this.parent) {
			this.parent.classList.add("hidden");
		}
	}

	controlID (controlName) {
		return `${controlName}_${this.__id_}`;
	}

	ID() {
		return this.__id_;
	}

	set_userGuideMenuWidgetWrapper (value) {
		let element = document.getElementById(`userGuideMenuWidgetWrapper_${this.__id_}`);
		if (element) {
			element.innerHTML = value;
		}
	}

	get_userGuideMenuWidgetWrapper () {
		let element = document.getElementById(`userGuideMenuWidgetWrapper_${this.__id_}`);
		if (element) {
			return element.innerHTML;
		}
		return null;
	}

}

function userGuideMenuWidget_purgeDeleted() {
	userGuideMenuWidgetInstances = userGuideMenuWidgetInstances.filter(userGuideMenuWidget => !userGuideMenuWidget.deleted);
}

function userGuideMenuWidget_remove(property = null, value = null) {
	let _userGuideMenuWidget = userGuideMenuWidget_all(property, value);
	if (!_userGuideMenuWidget || _userGuideMenuWidget.length === 0) {
		return null;
	}
	let removeEntries = property !== null && value !== null;
	_userGuideMenuWidget.forEach(widgetItem => {
		if (!widgetItem.deleted) {
			if (removeEntries) {
				widgetItem.deleted = true;
			}

			let widgetItemContainer = document.getElementById(widgetItem.__id_);
			if (widgetItemContainer && widgetItemContainer.parentNode) {
				widgetItemContainer.parentNode.removeChild(widgetItemContainer);
			}
		}
	});

	if (!removeEntries) {
		userGuideMenuWidgetInstances = [];
	}
}

function userGuideMenuWidget_count(property, value) {
	//	take this opportunity to clean up
	userGuideMenuWidget_purgeDeleted();
	let _userGuideMenuWidget = userGuideMenuWidgetInstances.filter(userGuideMenuWidget => userGuideMenuWidget[property] === value);
	return _userGuideMenuWidget ? _userGuideMenuWidget.length : 0;
}

function userGuideMenuWidget_one(property, value) {
	//	take this opportunity to clean up
	userGuideMenuWidget_purgeDeleted();
	let _userGuideMenuWidget = userGuideMenuWidgetInstances.filter(userGuideMenuWidget => userGuideMenuWidget[property] === value);
	return _userGuideMenuWidget ? _userGuideMenuWidget[0] : null;
}

function userGuideMenuWidget_all(property, value) {
	//	take this opportunity to clean up
	userGuideMenuWidget_purgeDeleted();
	let _userGuideMenuWidget = property ? userGuideMenuWidgetInstances.filter(userGuideMenuWidget => userGuideMenuWidget[property] === value): userGuideMenuWidgetInstances;
	return _userGuideMenuWidget ? _userGuideMenuWidget : null;
}

function userGuideMenuWidget_create (optionsHidden, section) {
	let _userGuideMenuWidget = new userGuideMenuWidget(optionsHidden);
	_userGuideMenuWidget.bind(section);
	userGuideMenuWidgetInstances.push(_userGuideMenuWidget);
	return _userGuideMenuWidget;
}

