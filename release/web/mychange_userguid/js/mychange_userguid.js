// Specify the URL where the api will be hosted. e.g. 'http://mydomain.com/myapi/'let MyChangePortalWs_apiURL = "$WEBAPI_URL$"; function  addAbbreviationBlocks(){let glossarySD_keys = glossarySD ? Object.keys(glossarySD) : null;if (glossarySD_keys) {for (let __glossarySDIdx = 0; __glossarySDIdx < glossarySD_keys.length; __glossarySDIdx++) {let glossarySD_key = glossarySD_keys[__glossarySDIdx];let glossaryW = glossaryWidget_create(glossarySD_key, glossarySD[glossarySD_key], "moreAbbreviationBlocks");}}} function  loadFromExternal(){let query = "";
query = Utils.getUrlQuery();UserRole = Utils.getUrlToken(query,"userRole");AuthToken = Utils.getUrlToken(query,"authToken");changeProcessController.load();} function  externalReturn(pageReturn){Utils.returnExternal(AuthToken,pageReturn);} function  showHideguidanceOptions(){let userGuideMenuW = userGuideMenuWidget_one();
if ( userGuideMenuW.optionsHidden === true ) {document.getElementById(userGuideMenuW.controlID("guidanceOptions_UGMW")).style.display = 'flex';
document.getElementById(userGuideMenuW.controlID("img_guidanceArrow_UGMW")).src = "./Images/upArrow_black.svg";userGuideMenuW.optionsHidden = false;}else{document.getElementById(userGuideMenuW.controlID("guidanceOptions_UGMW")).style.display = 'none';
document.getElementById(userGuideMenuW.controlID("img_guidanceArrow_UGMW")).src = "./Images/downArrow_black.svg";userGuideMenuW.optionsHidden = true;}} function  chooseMenuItem(item){unchooseMenuItems();
let userGuideMenuW = userGuideMenuWidget_one();
if ( item === "Change Process" ) {document.getElementById(userGuideMenuW.controlID("changeProcess_UGMW")).style.backgroundColor = "#E6E6E6";
changeProcessController.load();}else if ( item === "Change Process Tailoring" ) {document.getElementById(userGuideMenuW.controlID("changeProcessTailoring_UGMW")).style.backgroundColor = "#E6E6E6";
changeProcessTailoringController.load();}else if ( item === "MyChange Overview" ) {document.getElementById(userGuideMenuW.controlID("myChangeOverview_UGMW")).style.backgroundColor = "#E6E6E6";
overviewController.load();}else if ( item === "Roles & Responsibilities" ) {document.getElementById(userGuideMenuW.controlID("rolesAndResponsibilities_UGMW")).style.backgroundColor = "#E6E6E6";
rolesAndResponsibilitiesController.load();}else if ( item === "Help & FAQs" ) {document.getElementById(userGuideMenuW.controlID("helpAndFAQs_UGMW")).style.backgroundColor = "#E6E6E6";
helpAndFAQsController.load();}else if ( item === "Glossary" ) {document.getElementById(userGuideMenuW.controlID("glossary_UGMW")).style.backgroundColor = "#E6E6E6";
glossaryController.load();}else if ( item === "Guidance" ) {showHideguidanceOptions();
guidanceController.load();}} function  unchooseMenuItems(){let userGuideMenuW = userGuideMenuWidget_one();
document.getElementById(userGuideMenuW.controlID("changeProcess_UGMW")).style.backgroundColor = "#FFFFFF";
document.getElementById(userGuideMenuW.controlID("changeProcessTailoring_UGMW")).style.backgroundColor = "#FFFFFF";
document.getElementById(userGuideMenuW.controlID("myChangeOverview_UGMW")).style.backgroundColor = "#FFFFFF";
document.getElementById(userGuideMenuW.controlID("rolesAndResponsibilities_UGMW")).style.backgroundColor = "#FFFFFF";
document.getElementById(userGuideMenuW.controlID("helpAndFAQs_UGMW")).style.backgroundColor = "#FFFFFF";
document.getElementById(userGuideMenuW.controlID("glossary_UGMW")).style.backgroundColor = "#FFFFFF";
document.getElementById(userGuideMenuW.controlID("guidance_UGMW")).style.backgroundColor = "#FFFFFF";} function  showHideHelpAndFAQs(ID){let helpAndFAQsW = helpAndFAQsWidget_one("id", ID);
if ( helpAndFAQsW.bodyHidden === true ) {document.getElementById(helpAndFAQsW.controlID("helpAndFAQsWidgetBody")).style.display = 'block';
document.getElementById(helpAndFAQsW.controlID("img_helpAndFAQsWidgetHeader")).src = "./Images/remove_blue.svg";document.getElementById(helpAndFAQsW.controlID("helpAndFAQsWidgetHeader")).style.border = "1px solid #1E87C3";
document.getElementById(helpAndFAQsW.controlID("lbl_helpAndFAQsWidgetHeader")).style.color = "#1E87C3";
helpAndFAQsW.bodyHidden = false;}else{document.getElementById(helpAndFAQsW.controlID("helpAndFAQsWidgetBody")).style.display = 'none';
document.getElementById(helpAndFAQsW.controlID("img_helpAndFAQsWidgetHeader")).src = "./Images/add_black.svg";document.getElementById(helpAndFAQsW.controlID("helpAndFAQsWidgetHeader")).style.border = "0px solid #FFFFFF";
document.getElementById(helpAndFAQsW.controlID("lbl_helpAndFAQsWidgetHeader")).style.color = "#000000";
helpAndFAQsW.bodyHidden = true;}}function onmychange_userguidUIBodyLoad() {loadFromExternal();}