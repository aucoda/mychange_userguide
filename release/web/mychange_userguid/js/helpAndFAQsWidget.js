let helpAndFAQsWidgetInstances = [];

let helpAndFAQsWidget_id = 0;

class helpAndFAQsWidget {
	constructor (id, question, answer, bodyHidden) {
		this.__id_ = "helpAndFAQsWidget_" + helpAndFAQsWidget_id++;
		this.deleted = false;
		this.parent = null;

		this.id = id;
		this.question = question;
		this.answer = answer;
		this.bodyHidden = bodyHidden;

		this.template = `
			<div id="${this.__id_}">
				<section id='helpAndFAQsWidgetWrapper_${this.__id_}' class='helpAndFAQsWidgetWrapper_widget_style' >
					<section id='helpAndFAQsWidgetHeader_${this.__id_}' class='helpAndFAQsWidgetHeader_widget_style' onclick='event.stopPropagation(); showHideHelpAndFAQs(${this.id});'>
						<label id='lbl_helpAndFAQsWidgetHeader_${this.__id_}' class='lbl_helpAndFAQsWidgetHeader_widget_style' >${this.question}</label>
						<img id='img_helpAndFAQsWidgetHeader_${this.__id_}' src='./Images/add_black.svg' alt='' title='' class='img_helpAndFAQsWidgetHeader_widget_style'/>
					</section>
					<section id='helpAndFAQsWidgetBody_${this.__id_}' class='helpAndFAQsWidgetBody_widget_style' >
						<label id='lbl_helpAndFAQsWidgetBody_${this.__id_}' class='lbl_helpAndFAQsWidgetBody_widget_style' >${this.answer}</label>
					</section>
				</section>
			</div>
		`;
	}

	bind (sectionId) {
		let element = document.getElementById(sectionId);
		if (element) {
			element.insertAdjacentHTML("beforeend", this.template);
			this.parent = document.getElementById(this.__id_);
		}
	}

	show () {
		if (!this.parent) {
			this.bind();
		}

		this.parent.classList.add("hidden");
	}

	hide () {
		if (this.parent) {
			this.parent.classList.add("hidden");
		}
	}

	controlID (controlName) {
		return `${controlName}_${this.__id_}`;
	}

	ID() {
		return this.__id_;
	}

	set_helpAndFAQsWidgetWrapper (value) {
		let element = document.getElementById(`helpAndFAQsWidgetWrapper_${this.__id_}`);
		if (element) {
			element.innerHTML = value;
		}
	}

	get_helpAndFAQsWidgetWrapper () {
		let element = document.getElementById(`helpAndFAQsWidgetWrapper_${this.__id_}`);
		if (element) {
			return element.innerHTML;
		}
		return null;
	}

}

function helpAndFAQsWidget_purgeDeleted() {
	helpAndFAQsWidgetInstances = helpAndFAQsWidgetInstances.filter(helpAndFAQsWidget => !helpAndFAQsWidget.deleted);
}

function helpAndFAQsWidget_remove(property = null, value = null) {
	let _helpAndFAQsWidget = helpAndFAQsWidget_all(property, value);
	if (!_helpAndFAQsWidget || _helpAndFAQsWidget.length === 0) {
		return null;
	}
	let removeEntries = property !== null && value !== null;
	_helpAndFAQsWidget.forEach(widgetItem => {
		if (!widgetItem.deleted) {
			if (removeEntries) {
				widgetItem.deleted = true;
			}

			let widgetItemContainer = document.getElementById(widgetItem.__id_);
			if (widgetItemContainer && widgetItemContainer.parentNode) {
				widgetItemContainer.parentNode.removeChild(widgetItemContainer);
			}
		}
	});

	if (!removeEntries) {
		helpAndFAQsWidgetInstances = [];
	}
}

function helpAndFAQsWidget_count(property, value) {
	//	take this opportunity to clean up
	helpAndFAQsWidget_purgeDeleted();
	let _helpAndFAQsWidget = helpAndFAQsWidgetInstances.filter(helpAndFAQsWidget => helpAndFAQsWidget[property] === value);
	return _helpAndFAQsWidget ? _helpAndFAQsWidget.length : 0;
}

function helpAndFAQsWidget_one(property, value) {
	//	take this opportunity to clean up
	helpAndFAQsWidget_purgeDeleted();
	let _helpAndFAQsWidget = helpAndFAQsWidgetInstances.filter(helpAndFAQsWidget => helpAndFAQsWidget[property] === value);
	return _helpAndFAQsWidget ? _helpAndFAQsWidget[0] : null;
}

function helpAndFAQsWidget_all(property, value) {
	//	take this opportunity to clean up
	helpAndFAQsWidget_purgeDeleted();
	let _helpAndFAQsWidget = property ? helpAndFAQsWidgetInstances.filter(helpAndFAQsWidget => helpAndFAQsWidget[property] === value): helpAndFAQsWidgetInstances;
	return _helpAndFAQsWidget ? _helpAndFAQsWidget : null;
}

function helpAndFAQsWidget_create (id, question, answer, bodyHidden, section) {
	let _helpAndFAQsWidget = new helpAndFAQsWidget(id, question, answer, bodyHidden);
	_helpAndFAQsWidget.bind(section);
	helpAndFAQsWidgetInstances.push(_helpAndFAQsWidget);
	return _helpAndFAQsWidget;
}

