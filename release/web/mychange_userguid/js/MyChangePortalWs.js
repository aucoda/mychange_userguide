let MyChangePortalWs = {mergeHeaders: function (defaultHeaders, headers) {
    for (let h in headers) {
        if (headers.hasOwnProperty(h)) {
            if (typeof headers[h] !== "object") {
                defaultHeaders[h] = headers[h];
            }
            else {
                for (let hh in headers[h]) {
                    if (headers[h].hasOwnProperty(hh)) {
                        defaultHeaders[hh] = headers[h][hh];
                    }
                }
            }
        }
    }
    return defaultHeaders;
},getHelpAndFAQsWf: function (role,topic) {return new Promise (function (resolve, reject) {
            let url = MyChangePortalWs_apiURL + 'getHelpAndFAQsWf';
            let xhr = new XMLHttpRequest();
            xhr.open('POST', url, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(xhr.response);
                }
                else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }
            };
            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            };
            let data = {role: role,topic: topic};
            xhr.send(JSON.stringify(data));
        })},
downloadFileUserGuideWf: function (name) {return new Promise (function (resolve, reject) {
            let url = MyChangePortalWs_apiURL + 'downloadFileUserGuideWf';
            let xhr = new XMLHttpRequest();
            xhr.open('POST', url, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    let stringResponse = JSON.parse(this.responseText);
                    let urlParams = "?incomingFileValue=" + encodeURIComponent(stringResponse);
                    window.open(MyChangePortalWs_apiURL + 'getdownloadFileUserGuideWf' + urlParams);
                    resolve("{}");
                }
                else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }
            };
            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            };
            let data = {name: name};
            xhr.send(JSON.stringify(data));
        })},};