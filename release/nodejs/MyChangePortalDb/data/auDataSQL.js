/*
    auData
    
    auData - base class for au tables
    generates sql and runs it using auSQL
    
    Raj Curwen
    aucoda.com
    
    21/11/2017 Created
*/

"use strict";

let auSQLite = require("./auSQLite"),
    auCursor = require("./auCursor");

//  conversion object used for parameters to take js values and convert them to sqlite usable parameters

const convert = {
    boolean: function(value) {
        return value ? 1 : 0;
    },
    object: function(value) {
        if(value && Object.prototype.toString.call(value) === "[object Date]") {
            if(isNaN(value.getTime())) {
                //  we have an invalid date object, when converting set to 'null'
                return "null";
            }
            return value.toString("yyyy-mm-dd HH:MM:SS");
        }
        if (value && value.data) {
            return value.data;
        }
        return value ? value : "null";
    }
};

//  we only want one SQL db open for all tables
let sql = null;
function getSQLDB(database) {
    if (!sql) {
        sql = auSQLite.create(database);
    }

    return sql;
}

//  ----------------------------------
//  AU support - manages the sqlite db
module.exports = class auDataSQL {
    constructor(database) {
        this.ausql = getSQLDB(database);

        this.name = null;
        this.fieldCount = 0;
        this.fields = [];
        this.joinClauses = {};
        this.key = null;

        this.isCached = false;
        this.isComposite = false;

        this.cursor = null;

        this.keyWordFields = [
            "GROUP"
        ];
        //  paging
        //  these values are set by the derived class
        //  offset is maintained by the super class
        this.limit = null;
        this.offset = 0;
    }

    cache(sql) {
        this.ausql.cache(sql);
    }

    commitSQLCache(callback) {
        this.ausql.commitSQLCache(callback);
    }



    toField(fieldName) {

        let upperCasedName = fieldName.toUpperCase();
        if(this.keyWordFields.indexOf(upperCasedName) >= 0) {
            return `[${fieldName}]`;
        }
        return fieldName;
    }

    insertIntoTable(table, fields, values, callback) {
        let parameterisedValues = [];
        for (let i=0; i < fields.length; i++) {
            let fieldItems = fields[i].split(" = ");
            fields[i] = this.toField(fieldItems[0]);
            parameterisedValues.push("?");
        }

        let sql = `INSERT into ${table} (${fields.join(",")}) VALUES (##PARAMETERISED_VALUES##);`;

        sql = sql.replace("##PARAMETERISED_VALUES##", parameterisedValues.join(", "));

        // loop over the values and convert any that are being inserted to the correct value for the database
        if(values && values.length) {
            for(let i=0; i < values.length; i++) {
                let value = values[i];
                if(convert.hasOwnProperty(typeof value)) {
                    values[i] = convert[typeof value](value);
                }
            }
        }

        this.ausql.insert(sql, values, callback);
    }

    insert(fields, fieldParameters, callback) {
        this.insertIntoTable(this.name, fields, fieldParameters, callback);
    }

    update(fields, values, condition, callback) {
        let sql = `UPDATE ${this.name} SET 
            ##SETS##
            WHERE ${condition};`;

        let sets = fields;
        // loop over the values and convert any that are being inserted to the correct value for the database
        if(values && values.length) {
            for(let i=0; i < values.length; i++) {
                let value = values[i];
                if(convert.hasOwnProperty(typeof value)) {
                    values[i] = convert[typeof value](value);
                }
            }
        }

        sql = sql.replace("##SETS##", sets.join(","));
        this.ausql.update(sql, values, callback);
    }

    updateEntity(fields, values, condition, callback) {
        let sql = `UPDATE ${this.name} SET 
            ##SETS##
            WHERE ${condition};`;

        let sets= [];
        for (let i=0; i < fields.length; i++) {
            fields[i] = this.toField(fields[i]);
            sets.push(`${fields[i]} = ?`);
        }

        // loop over the values and convert any that are being inserted to the correct value for the database
        if(values && values.length) {
            for(let i=0; i < values.length; i++) {
                let value = values[i];
                if(convert.hasOwnProperty(typeof value)) {
                    values[i] = convert[typeof value](value);
                }
            }
        }

        sql = sql.replace("##SETS##", sets.join(","));
        this.ausql.update(sql, values, callback);
    }

    _delete(condition, parameters, callback) {
        let sql = `DELETE FROM ${this.name} WHERE ${condition}`;
        this.ausql._delete(sql, parameters, callback);
    }

    deleteAll(callback) {
        let sql = `DELETE FROM ${this.name}`;
        this.ausql.deleteAll(sql, callback);
    }

    count(dataSource, condition, parameters, joinTables, callback) {
        let sql = `SELECT COUNT(*) numOfRows FROM ${this.name}`;

        //  work out the joins
        if (joinTables && joinTables.length) {
            let joins = [];
            //  mix in any joins
            for (let i = 0; i < joinTables.length; i++) {
                if (this.joinClauses.hasOwnProperty(joinTables[i])) {
                    joins.push(this.joinClauses[joinTables[i]]);
                }
            }

            if (joins.length) {
                sql = sql + "\r\n" + joins.join("\r\n");
            }
        }
        //  loop over the parameters and convert any that are required incase sqlite wont understand them, e.g. true is
        //  stored as an integer so needs to be 1
        if(parameters && parameters.length) {
            for(let j=0; j < parameters.length; j++) {
                let parameter = parameters[j];
                if(convert.hasOwnProperty(typeof parameter)) {
                    parameters[j] = convert[typeof parameter](parameter);
                }
            }
        }
        if (condition) {
            sql = sql + " where " + condition;
        }
        this.ausql.select(sql, parameters, (rows) => {
            if (callback) {
                // fetch data value from the first row
                let numOfRows = rows[0].numOfRows;
                callback(numOfRows);
            }
        });
    }

    select(dataSource, fields, condition, parameters, joinTables, orderBy, parentJoin, callback) {
        let sql = "select " + fields + " from " + this.name;

        //  work out the joins
        if (joinTables && joinTables.length) {
            let joins = [];
            //  mix in any joins
            for (let i = 0; i < joinTables.length; i++) {
                if (this.joinClauses.hasOwnProperty(joinTables[i])) {
                    joins.push(this.joinClauses[joinTables[i]]);
                }
            }

            if(parentJoin) {
                joins.push(parentJoin);
            }

            if (joins.length) {
                sql = sql + "\r\n" + joins.join("\r\n");
            }
        }
        //  loop over the parameters and convert any that are required incase sqlite wont understand them, e.g. true is
        //  stored as an integer so needs to be 1
        if(parameters && parameters.length) {
            for(let j=0; j < parameters.length; j++) {
                let parameter = parameters[j];
                if(convert.hasOwnProperty(typeof parameter)) {
                    parameters[j] = convert[typeof parameter](parameter);
                }
            }
        }
        // add any conditions
        if (condition) {
            sql = sql + " where " + condition;
        }

        // apply order by clause(s), asc/desc needs is included in the orderBy string
        if (orderBy) {
            sql = sql + " order by " + orderBy;
        }

        // add paging info
        if (this.limit !== null) {
            sql = sql + " LIMIT " + this.limit + " OFFSET " + this.offset;
        }

        this.ausql.select(sql, parameters, (rows) => {
            if (callback) {
                if (this.limit) {
                    //  update the next start offset
                    this.offset += this.limit;
                }
                callback(new auCursor(rows));
            }
        });

    }
};